import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName =
    position === "right" ? "fighter-preview___right" : "fighter-preview___left";
  const fighterElement = createElement({
    tagName: "div",
    className: `fighter-preview___root ${positionClassName}`,
  });

  const { name, health, attack, defense } = fighter;
  const fighterImage = createFighterImage(fighter);
  if (positionClassName === "fighter-preview___right") {
    fighterImage.classList.add("flip-horizontal");
  }

  const fighterInfo = createElement({
    tagName: "div",
    className: "fighterInfo",
  });

  const fighterStats = createElement({
    tagName: "div",
    className: "fighterStats",
  });

  fighterStats.innerHTML = "Stats:";

  const fighterName = createElement({
    tagName: "p",
    className: "fighterName",
  });

  fighterName.innerHTML = `Name: ${name}`;

  const fighterHealth = createElement({
    tagName: "p",
    className: "fighterHp",
  });
  fighterHealth.innerHTML = `Hp: ${health}`;

  const fighterAttack = createElement({
    tagName: "p",
    className: "fighterAttack",
  });
  fighterAttack.innerHTML = `Attack: ${attack}`;

  const fighterDefense = createElement({
    tagName: "p",
    className: "fighterAttack",
  });

  fighterDefense.innerHTML = `Defense: ${defense}`;

  fighterInfo.append(
    fighterStats,
    fighterName,
    fighterHealth,
    fighterAttack,
    fighterDefense
  );
  fighterElement.append(fighterImage, fighterInfo);
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
