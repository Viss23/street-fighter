import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
	return new Promise((resolve) => {
		const changeFirstFighterHpBar = updateLeftFighterHpBar(firstFighter.health);
		const changeSecondFighterHpBar = updateRightFighterHpBar(secondFighter.health);
		let firstPlayerComboCanBeUsedInTime = 0;
		let secondPlayerComboCanBeUsedInTime = 0;

		let pressed = new Set();

		const fightCallback1 = function (event) {
			pressed.add(event.code);

      let firstFighterAttack = pressed.has(controls.PlayerOneAttack) && !pressed.has(controls.PlayerOneBlock) && 
        !pressed.has(controls.PlayerTwoBlock);
      let secondFighterAttack = pressed.has(controls.PlayerTwoAttack) && !pressed.has(controls.PlayerTwoBlock) && 
        !pressed.has(controls.PlayerOneBlock);
			let firstFighterUseCombo =
				controls.PlayerOneCriticalHitCombination.every((element) => pressed.has(element)) &&
        firstPlayerComboCanBeUsedInTime < Math.floor(Date.now() / 1000) &&
        !pressed.has(controls.PlayerOneBlock);
			let secondFighterUseCombo =
				controls.PlayerTwoCriticalHitCombination.every((element) => pressed.has(element)) &&
        secondPlayerComboCanBeUsedInTime < Math.floor(Date.now() / 1000) &&
        !pressed.has(controls.PlayerTwoBlock);

			if (firstFighterUseCombo) {
				firstPlayerComboCanBeUsedInTime = Math.floor(Date.now() / 1000) + 10;
				changeSecondFighterHpBar(secondFighter, specialAttack(firstFighter));
				if (isFighterDead(secondFighter)) {
					document.removeEventListener('keydown', fightCallback1);
					document.removeEventListener('keyup', fightCallback2);
					resolve(firstFighter);
				}
			} else if (secondFighterUseCombo) {
				secondPlayerComboCanBeUsedInTime = Math.floor(Date.now() / 1000) + 10;
				changeFirstFighterHpBar(firstFighter, specialAttack(secondFighter));
				if (isFighterDead(firstFighter)) {
					document.removeEventListener('keydown', fightCallback1);
					document.removeEventListener('keyup', fightCallback2);
					resolve(secondFighter);
				}
			} else if (firstFighterAttack) {
				changeSecondFighterHpBar(secondFighter, getDamage(firstFighter, secondFighter));
				if (isFighterDead(secondFighter)) {
					document.removeEventListener('keydown', fightCallback1);
					document.removeEventListener('keyup', fightCallback2);
					resolve(firstFighter);
				}
			} else if (secondFighterAttack) {
				changeFirstFighterHpBar(firstFighter, getDamage(secondFighter,firstFighter));
				if (isFighterDead(firstFighter)) {
					document.removeEventListener('keydown', fightCallback1);
					document.removeEventListener('keyup', fightCallback2);
					resolve(secondFighter);
				}
			}
		};

		const fightCallback2 = function (event) {
			pressed.delete(event.code);
		};

		document.addEventListener('keydown', fightCallback1);
		document.addEventListener('keyup', fightCallback2);
	});
}

export function getDamage(attacker, defender) {
	const damage = getHitPower(attacker) - getBlockPower(defender);
	return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
	const criticalHitChance = Math.random() + 1;
	const hitPower = fighter.attack * criticalHitChance;
	return hitPower;
}

export function getBlockPower(fighter) {
	const dodgeChance = Math.random() + 1;
	const blockPower = fighter.defense * dodgeChance;
	return blockPower;
}

export function specialAttack(attacker) {
	let damage = attacker.attack * 2;
	return damage;
}

export function updateLeftFighterHpBar(maxHp) {
	return function (fighter, damage) {
    let currentHp = fighter.health - damage;
    fighter.health = currentHp;
		let percentOfMaxHp = (currentHp / maxHp) * 100;
		if (percentOfMaxHp < 0) percentOfMaxHp = 0;
		document.getElementById('left-fighter-indicator').style.width = `${percentOfMaxHp}%`;
	};
}

export function updateRightFighterHpBar(maxHp) {
	return function (fighter, damage) {
		let currentHp = fighter.health - damage;
		fighter.health = currentHp;
		let percentOfMaxHp = (currentHp / maxHp) * 100;
		if (percentOfMaxHp < 0) percentOfMaxHp = 0;
		document.getElementById('right-fighter-indicator').style.width = `${percentOfMaxHp}%`;
	};
}

export function isFighterDead(fighter) {
	if (fighter.health <= 0) {
		return true;
	}
	return false;
}